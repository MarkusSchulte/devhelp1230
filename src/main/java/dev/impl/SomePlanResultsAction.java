package dev.impl;

import com.atlassian.bamboo.build.PlanResultsAction;
import com.opensymphony.xwork2.Action;

public class SomePlanResultsAction extends PlanResultsAction {

  @Override
  public String execute() {
    return Action.SUCCESS;
  }
}
